Introduction
==
Python scripts to extract & calculate scoring statistics from ESPN Fantasy Football league pages.
If will print a report that can be used to copy/paste on ESPN league page, or email. It also exports raw data to .csv for further analysis if desired.

The report includes:
 - Predicted Wins
   - Thanks to https://www.reddit.com/r/fantasyfootball/comments/5gtpbl/using_points_for_do_determine_how_well_youre/
 - Total Points Scored
 - Total Scored + Bench Points
 - Total Bench Points


 LICENSE: [MIT](LICENSE)

Requirements
============
 - python 2.7
 - requests
 - BeautifulSoup
 - pandas

The Anaconda python distribution is recommended to obtain above.



Quick Start
===========

## League Setup

 1. Only works for ESPN Head-to-Head leagues, and you must make League Viewable to Public in ESPN league settings. Otherwise scripts won't work.

 2. Edit [league.json](league.json) with basic your ESPN league info.
    look for the leagueId in your ESPN league home page URL.

    Example:
    ```json
    {
    "leagueID": 99999,
    "seasonID": 2016
    }
    ```

 3. Run script to update json with more complete information.

    `% python get_EPSN_teamnames.py league.json`

    This will produce a leagueinfo_updated.json with additional fields.
    - full teams names:
    - short team names
    - team id number (IDs aren't sequential in some leagues)
    This only needs done once per league, and the resulting .json is used in the following report generation script.

    Example [updated league .json](examples/example_league_updated.json)

## Report Generation

The report is generated via the following script usage:

`% python gen_report_ESPN.py leagueinfo_updated.json`

The report is just printed to STDOUT such that it can be directly copy/pasted into the ESPN league front page - or an email.

A CSV file of the raw data is also created in the form of `league{LEAGUEID}_scores{YEAR}_wks{NUMWEEKS}.csv`
which can be used for further analysis.

Example output:
 - [Report](examples/example_report.txt)
 - [CSV](examples/example_leagueNNNNN_scores2016_wks14.csv)


*Optional*: The number of weeks can be provided. Example to for the first 4 weeks:

`% python gen_report_ESPN.py leagueinfo_updated.json 4`

Without it, the script will calculate the number of completed weeks/games based on date the script is ran.
