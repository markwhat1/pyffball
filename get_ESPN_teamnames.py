#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 17 08:10:27 2016

@author: bscipio
"""

import urllib2
import requests
from bs4 import BeautifulSoup

import collections


def read_json(jsonfn):
    import json
    with open(jsonfn) as json_data:
        d = json.load(json_data) 
    return d

def write_json(d,outfn="output.json"):
    import json
    with open(outfn, 'w') as fp:
        json.dump(d, fp)
    

def requests_soup(URL):
    #Query the website and return the html to the variable 'page'
    #page = urllib2.urlopen(URL)
    
    result = requests.get(URL)
    page = result.content
    
    #Parse the html in the 'page' variable, and store it in Beautiful Soup format
    soup = BeautifulSoup(page, "lxml")
    return soup


def getESPNteams(leagueId,seasonId, verbose=True):
    
    league_url = ("http://games.espn.com/ffl/leagueoffice?leagueId={}"
                "&seasonId={}".format(leagueId,seasonId))
    
    #print boxscore_url

    soup = requests_soup(league_url)
    
    gt = soup.find("ul", id="games-tabs1")
    
    nteams = len(gt)
    
    team_li = gt.find_all('li')
    
    teams_full = []
    teams_short = []
    teams_ids = []
    for t in team_li:
        tl = t.text.split("(")
        tf = tl[0].strip()
        ts = tl[1][:-1] #removes trailing ")"

        # get the teamID number since ESPN ids aren't always sequential
        thref = t.find("a")['href'].split("&")
        for s in thref:
            if 'teamId=' in s:
                teamID = int(s.strip("teamId="))
    


        
        teams_full.append(tf)
        teams_short.append(ts)
        teams_ids.append(teamID)


    return teams_full,teams_short,teams_ids

    
def main(leagueinfo,outjson="updated.json"):
    leagueId = leagueinfo["leagueID"]
    seasonId = leagueinfo["seasonID"]

    tf,ts,ti = getESPNteams(leagueId,seasonId, verbose=True)
    
    # update league team name info
    leagueinfo["teamNames"] = tf
    leagueinfo["shortNames"] = ts
    leagueinfo["teamIDs"] = ti
        
    write_json(leagueinfo,outjson)
    
    from pprint import pprint
    print("### Updated League Info ####")
    pprint(leagueinfo)

    print("\nUpdated League .json file: {}".format(outjson))
    
if __name__ == "__main__":

    import sys,os

    if len(sys.argv) < 2:
        print("{} [League json Filename]".format(sys.argv[0]))
        sys.exit(1)
    else:
        infn = sys.argv[1]
        leagueinfo = read_json(infn)
        
        updatedfn = os.path.splitext(infn)[0] + "_updated.json" 
        
        main(leagueinfo,updatedfn)
        





    
    

