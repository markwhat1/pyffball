#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 18:38:29 2016

@author: bscipio
"""

#import urllib2
import requests
from bs4 import BeautifulSoup

import collections

import pandas as pd



def requests_soup(URL):
    #Query the website and return the html to the variable 'page'
    #page = urllib2.urlopen(URL)
    
    result = requests.get(URL)
    page = result.content
    
    #Parse the html in the 'page' variable, and store it in Beautiful Soup format
    soup = BeautifulSoup(page, "lxml")
    return soup


def getESPNscore(scoringPeriodId,teamId,leagueId,seasonId, verbose=True):
    boxscore_url = ("http://games.espn.com/ffl/boxscorequick?leagueId={}"
                "&teamId={}&scoringPeriodId={}&seasonId={}"
                "&view=scoringperiod&version=quick".format(leagueId,teamId,scoringPeriodId,seasonId))

    #print boxscore_url

    soup = requests_soup(boxscore_url)
    
    
    # Get Real Points
    #<div class="danglerBox totalScore" title="119">119</div>
    #<div class="danglerBox totalScore" title="94">94</div>
    ptssp = soup.find("div",class_="danglerBox totalScore")
    pts = float(ptssp.contents[0])
    
    # Get Bench Points
    #<div class="danglerBox" id="tmInactivePts_2">95</div>
    benchsp = soup.find("div",id="tmInactivePts_{}".format(teamId))
    benchpts = float(benchsp.contents[0])
    
#    if verbose:
#        reportstr = "{tn}, {wk}, {pts}, {bn}".format(tn=teamNames[teamId-1],
#                                                  wk=scoringPeriodId,
#                                                  pts=pts,
#                                                  bn=benchpts)
#
#        print(reportstr)

    return  pts, benchpts

def getweeknum():
    from datetime import date
    import math
    # TODO Find better way to define start of season
    d0 = date(2017, 9, 6)  # start of season   EDIT AS NEEDED
    d1 = date.today()  
    delta = d1 - d0
    nweeks = int(math.floor(delta.days/7.))
    return nweeks

def read_json(jsonfn):
    import json
    with open(jsonfn) as json_data:
        d = json.load(json_data) 
    return d
    
def main(leagueinfo,n_complete_wks=None):
        
    leagueId = leagueinfo["leagueID"]
    seasonId = leagueinfo["seasonID"]
    teamNames =  leagueinfo["teamNames"]
    teamIDs = leagueinfo["teamIDs"]
    
    #==============================================================================
    # Scrape ESPN
    #==============================================================================
    
    # guess number of completed weeks if not provided
    if n_complete_wks == None:
        #n_complete_wks = 16
        n_complete_wks = getweeknum()  
        #n_complete_wks = 3
    
        
    weeks = range(1,n_complete_wks+1)
    
    
    teamlist = []
    teamIds = []
    weeknum = []
    teamscore = []
    teambench = []
    teamshortname = []
    
    #for teamId in range(1,13):
    #for teamId in range(1,13):
    for i,teamId in enumerate(teamIDs):

        for scoringPeriodId in weeks:
        
            pts, benchpts = getESPNscore(scoringPeriodId,teamId,leagueId,seasonId, verbose=False)
    
            teamlist.append(teamNames[i]) 
            teamIds.append(teamId)
            teamshortname.append(leagueinfo["shortNames"][i])
            teamscore.append(pts)
            teambench.append(benchpts)
            weeknum.append(scoringPeriodId)
    
    scoredata = collections.OrderedDict()
    
    scoredata["team"] = teamlist
    scoredata["teamId"] = teamIds
    scoredata["shortnames"] = teamshortname
    scoredata["week"] = weeknum
    scoredata["score"] = teamscore
    scoredata["bench"] = teambench
    
    sdf = pd.DataFrame.from_dict(scoredata)
    
    #==============================================================================
    # Calculate ESPN
    #==============================================================================
    
    # Basic Stats
    
    #sdf["benchdiff"] = sdf["score"] - sdf["bench"]
    sdf["pts+bench"] = sdf["score"] + sdf["bench"]
    print(sdf)
    
    # Export data to .csv
    sdf.to_csv("league{}_scores{}_wks{}.csv".format(leagueId,seasonId, n_complete_wks),index_label="index")
    
    gb = sdf.groupby("team")
    
    summary = gb.sum()
    summary.drop('week', axis=1, inplace=True)
    summary.drop('teamId', axis=1, inplace=True)
    summary["ptsback"] = summary["score"].max() - summary["score"]
    
    #stdev = gb["score"].std()
    
    ## Expected Wins
    ##  prob_win each week
    ##  Ref: https://www.reddit.com/r/fantasyfootball/comments/5gtpbl/using_points_for_do_determine_how_well_youre/
    nteams = sdf.groupby("team").ngroups
    sdf["p_winwk"] = sdf.groupby("week")["score"].rank(method="first",ascending=True) - 1. 
    sdf["p_winwk"] /= nteams - 1
    
    
    tdf = sdf.groupby("team")["p_winwk"].sum()
    
#    print("Sort by Score Std.Deviation")
#    print(stdev.sort_values())
    
    print("""
==============================================================================
START LEAGUE PAGE SUMMARY REPORT
==============================================================================
""")
    
    print("<h2>Expected Wins in {} Weeks</h2>".format(n_complete_wks))
    print("""
    An estimate of the number of wins in a schedule-agnostic sense. 
    Reference : https://www.reddit.com/r/fantasyfootball/comments/5gtpbl/using_points_for_do_determine_how_well_youre/
     
    The probability that each team would have one that week, or "expected wins" is calculated.
    For example, in a 12-team league, the week's top scorer gets 12/12=1 win, second-highest gets 11/12=.916, 
    next highest gets .83, and so on. 
    
    Then those predicted wins are totaled for the season here:
    """)
    print("<pre>")
    print(tdf.sort_values(ascending=False).to_string(float_format="%.1f"))
    print("</pre>")
    
    
    print("<h2>Power Rank: by Total Scored Points over {} Weeks</h2>".format(n_complete_wks))
    print("<pre>")
    print(summary.sort_values(by="score", ascending=False))
    print("</pre>")
    
    print("<h2>Bench Rank: Total Bench Points over {} Weeks</h2>".format(n_complete_wks))
    print("""
    If only your bench counted...
    """)
    print("<pre>")
    print(summary.sort_values(by="bench", ascending=False)["bench"].to_string())
    print("</pre>")
    
    print("<h2>Power Rank: Combined Starters & Bench Points over {} Weeks</h2>".format(n_complete_wks))
    print("""
    Indicates combined strength of starters & bench 
    """)
    print("<pre>")
    print(summary.sort_values(by="pts+bench", ascending=False)["pts+bench"].to_string())
    print("</pre>")
    
#    print("<h2>Difference between Starters & Bench Points over {} Weeks</h2>".format(n_complete_wks))
#    print("""
#    If your benchdiff goes negative you're probably not starting the right guys ;)
#    """)
#    print("<pre>")
#    print(summary.sort_values(by="benchdiff", ascending=False))
#    print("</pre>")


if __name__ == "__main__":

    import sys      
  
    if len(sys.argv) < 2:
        print("{} [League json Filename] (Optional: # of weeks)".format(sys.argv[0]))
    else:
        leagueinfo = read_json(sys.argv[1])
        
        if len(sys.argv) > 2:
            n_complete_wks = int(sys.argv[2])
        else: 
            n_complete_wks = None
        
        main(leagueinfo,n_complete_wks)
